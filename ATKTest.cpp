#include "ATKTest.h"
#include "IPlug_include_in_plug_src.h"
#include "IControl.h"
#include "resource.h"

const int kNumPrograms = 1;

enum EParams
{
  kGain = 0,
  kNumParams
};

enum ELayout
{
  kWidth = GUI_WIDTH,
  kHeight = GUI_HEIGHT,

  kGainX = 100,
  kGainY = 100,
  kKnobFrames = 60
};

ATKTest::ATKTest(IPlugInstanceInfo instanceInfo)
  :	IPLUG_CTOR(kNumParams, kNumPrograms, instanceInfo)
{
  TRACE;

  IGraphics* pGraphics = MakeGraphics(this, kWidth, kHeight);
  pGraphics->AttachPanelBackground(&COLOR_RED);

  AttachGraphics(pGraphics);

  //MakePreset("preset 1", ... );
  MakeDefaultPreset((char *) "-", kNumPrograms);
}

ATKTest::~ATKTest() {}

void ATKTest::ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames)
{
  ATKTestProcessor * proc = new ATKTestProcessor();

  // Mutex is already locked for us.

  double* in1 = inputs[0];
  double* in2 = inputs[1];
  double* scin1 = inputs[2];
  double* scin2 = inputs[3];
  double* out1 = outputs[0];
  double* out2 = outputs[1];

  // Construct cutoff modulation
  std::vector<double> * cutoff = new std::vector<double>(nFrames);
  for (int i = 0; i < nFrames; i++) {
	  cutoff->push_back(2500.0);
  }

  proc->inpL.set_pointer(in1, nFrames);
  proc->inpR.set_pointer(in2, nFrames);

  proc->cutoffL.set_pointer(cutoff->data(), nFrames);
  proc->cutoffR.set_pointer(cutoff->data(), nFrames);

  proc->outpL.set_input_port(0, &proc->inpL, 0);
  proc->outpR.set_input_port(0, &proc->inpR, 0);

  /*proc->envfolL.set_attack(std::exp(-1e3 / 10.0 * GetSampleRate()));
  proc->envfolR.set_attack(std::exp(-1e3 / 10.0 * GetSampleRate()));

  proc->envfolL.set_release(std::exp(-1e3 / (100.0 * GetSampleRate())));
  proc->envfolR.set_release(std::exp(-1e3 / (100.0 * GetSampleRate())));*/
 
  proc->outpL.set_pointer(out1, nFrames); // Should get highpassed output?
  proc->outpR.set_pointer(out2, nFrames);

  proc->outpL.process(nFrames);
  proc->outpR.process(nFrames);
}

void ATKTest::Reset()
{
  TRACE;
  IMutexLock lock(this);
}

void ATKTest::OnParamChange(int paramIdx)
{
  IMutexLock lock(this);

  switch (paramIdx)
  { 
    default:
      break;
  }
}
