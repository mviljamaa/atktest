#include "ATKTestProcessor.h"


ATKTestProcessor::ATKTestProcessor() :
  inpL(0, 1, 0, false)
, inpR(0, 1, 0, false)
, cutoffL(0, 1, 0, false)
, cutoffR(0, 1, 0, false)
, outpL(0, 1, 0, false)
, outpR(0, 1, 0, false)
{

	inpL.set_input_sampling_rate(44100);
	inpL.set_output_sampling_rate(44100);
	inpR.set_input_sampling_rate(44100);
	inpR.set_output_sampling_rate(44100);

	cutoffL.set_input_sampling_rate(44100);
	cutoffL.set_output_sampling_rate(44100);
	cutoffR.set_input_sampling_rate(44100);
	cutoffR.set_output_sampling_rate(44100);

	ATKfiltL.set_input_sampling_rate(44100);
	ATKfiltL.set_output_sampling_rate(44100);
    
	//ATKfiltL.set_Q(1);
	ATKfiltL.set_min_frequency(20);
	ATKfiltL.set_max_frequency(20000);
	ATKfiltL.set_number_of_steps(1000);

	ATKfiltL.set_input_port(0, &inpL, 0);
	ATKfiltL.set_input_port(1, &cutoffL, 0);

	ATKfiltR.set_input_sampling_rate(44100);
	ATKfiltR.set_output_sampling_rate(44100);
	
	//ATKfiltR.set_Q(1);
	ATKfiltR.set_min_frequency(20);
	ATKfiltR.set_max_frequency(20000);
	ATKfiltR.set_number_of_steps(1000);

	ATKfiltR.set_input_port(0, &inpR, 0);
	ATKfiltR.set_input_port(1, &cutoffR, 0);

	/*powerL.set_input_sampling_rate(44100);
	powerL.set_output_sampling_rate(44100);
	powerL.set_input_port(0, &ATKfiltL, 0);
	powerR.set_input_sampling_rate(44100);
	powerR.set_output_sampling_rate(44100);
	powerR.set_input_port(0, &ATKfiltR, 0);

	envfolL.set_input_sampling_rate(44100);
	envfolL.set_output_sampling_rate(44100);
	envfolL.set_input_port(0, &powerL, 0);
	envfolR.set_input_sampling_rate(44100);
	envfolR.set_output_sampling_rate(44100);
	envfolR.set_input_port(0, &powerR, 0);*/

	outpL.set_input_sampling_rate(44100);
	outpL.set_output_sampling_rate(44100);
	outpL.set_input_port(0, &ATKfiltL, 0);
	outpR.set_input_sampling_rate(44100);
	outpR.set_output_sampling_rate(44100);
	outpR.set_input_port(0, &ATKfiltR, 0);

	/*inpL->full_setup();
	inpR->full_setup();
	powerL.full_setup();
	powerR.full_setup();
	envfolL.full_setup();
	envfolR.full_setup();
	outpL->full_setup();
	outpR->full_setup();*/
}

ATKTestProcessor::~ATKTestProcessor(){}