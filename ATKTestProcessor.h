#ifndef __ATKTestProcessor__
#define __ATKTestProcessor__

#include <ATK/Core/InPointerFilter.h>
#include <ATK/Core/OutPointerFilter.h>
#include <ATK/Dynamic/AttackReleaseFilter.h>
#include <ATK/Dynamic/PowerFilter.h>
#include <ATK/EQ/TimeVaryingSecondOrderFilter.h>

class ATKTestProcessor {
public:

	ATKTestProcessor::ATKTestProcessor();
	ATKTestProcessor::~ATKTestProcessor();

	ATK::InPointerFilter<double> inpL;
	ATK::InPointerFilter<double> inpR;
	ATK::InPointerFilter<double> cutoffL;
	ATK::InPointerFilter<double> cutoffR;
	ATK::TimeVaryingHighPassCoefficients<double> ATKfiltL;
	ATK::TimeVaryingHighPassCoefficients<double> ATKfiltR;
	// ATK::PowerFilter<double> powerL;
	// ATK::PowerFilter<double> powerR;
	// ATK::AttackReleaseFilter<double> envfolL;
	// ATK::AttackReleaseFilter<double> envfolR;
	ATK::OutPointerFilter<double> outpL;
	ATK::OutPointerFilter<double> outpR;
};

#endif
