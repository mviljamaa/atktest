#ifndef __ATKTest__
#define __ATKTest__

#include "IPlug_include_in_plug_hdr.h"
#include <vector>
#include "ATKTestProcessor.h"

class ATKTest : public IPlug
{
public:
  ATKTest(IPlugInstanceInfo instanceInfo);
  ~ATKTest();

  void Reset();
  void OnParamChange(int paramIdx);
  void ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames);

};

#endif
